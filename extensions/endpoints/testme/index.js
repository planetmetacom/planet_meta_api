module.exports = function registerEndpoint(router, { services, exceptions }) {
	router.get('/', (req, res) => res.send('Hello custom route'));
	router.get('/intro', (req, res) => res.send('Nice to meet you.'));
	router.get('/goodbye', (req, res) => res.send('Goodbye!'));
};